import { ApiFetch } from "./ajax.js";
import { Modal } from "./modal.js";
import { dragCards } from "./dragcards.js";
import { chooseRenderDoctor } from "./getCards.js";
import {root} from "./elements.js"
class Visit {
  constructor({
    personality,
    doctor,
    purpose,
    description,
    urgency,
    id,
    visitData,
  }) {
    this.personality = personality;
    this.doctor = doctor;
    this.purpose = purpose;
    this.description = description;
    this.urgency = urgency;
    this.id = id;
    this.visitData = visitData;
    this.template = document.querySelector(".visit").content;
    this.liTemplate = this.template.querySelector(".visit__menu");
    this.li = this.liTemplate.cloneNode(true);
    this.infoVisit = this.li.querySelector(".text-wrapper");
    this.imgVisit = this.li.querySelector(".visit__img");
    this.btnClose = this.li.querySelector("#btn-close");
    this.btnEdit = this.li.querySelector("#btn-edit");
    this.iconDone = this.li.querySelector(".icon-done");
    this.btnMoreLess = this.li.querySelector(".visit-btn");
  }

  render() {
    this.li.dataset.id = this.id;
    const nameVisit = this.li.querySelector(".visit__title");
    nameVisit.textContent = this.personality;

    const doctorVisit = this.li.querySelector(".visit__text-doctor");
    doctorVisit.textContent = this.doctor;
    const list = document.querySelector(".visit__list");

    this.btnClose.addEventListener("click", () => this.delete());

    this.btnEdit.addEventListener('click', () => {
        if (!root.hasChildNodes()) {
        const modalInstance = new Modal(this.btnEdit, this.doctor)
          modalInstance.createModal(this)
    }
    })
    list.append(this.li);
    this.li.setAttribute('draggable', true)
    dragCards(list)
  }

  showMoreLess() {
    this.infoVisit.hidden = true;
    this.infoVisit.innerHTML = `
    <p class="text__total">Мета: ${this.purpose} .</p>
    <p class="text__total">Короткий опис: ${this.description} .</p>
    <p class="text__total">Терміновість: ${this.urgency} .</p>
    `;

    this.btnMoreLess.addEventListener("click", () => {
      if (this.infoVisit.hidden) {
        this.btnMoreLess.textContent = "Приховати";
        this.infoVisit.hidden = false;
      } else {
        this.btnMoreLess.textContent = "Показати більше";
        this.infoVisit.hidden = true;
      }
    });

  }

  static edit(visit) {
    const list = document.querySelector(".visit__list");
    this.li = list.querySelector(`[data-id = "${visit.id}"]`);
    const li = chooseRenderDoctor(visit);
    this.li.before(li);
    this.li.remove();
  }

  delete() {
    ApiFetch.deleteCard(this.id)
      .then (this.li.remove());
  }
}

export {Visit}
