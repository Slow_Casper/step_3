import { ModalFormInput } from "./ModalForm.js";
import { Elements } from "./elements.js";

class ModalFormInputTerapevt extends ModalFormInput {
  constructor(form) {
    super();
    this.form = form;
  }
  createInputFieldTerapevt() {
    const Age = Elements.createFields("input", "number", "вік", "age");
    this.form.append(...this.createInputField(), ...Age);
  }
}
export {ModalFormInputTerapevt}
