import { Elements } from "./elements.js";

class ModalFormInput {
    createInputField() {
    const dateVisit = Elements.createFields(
        'input',
        'date',
        'дата візиту',
        "visitData")
    const Purpose = Elements.createFields(
      "input",
      "text",
      "мета візиту",
      "purpose"
    );
    const Description = Elements.createFields(
      "input",
      "text",
      "короткий опис візиту",
      "description"
    );
    const Personality = Elements.createFields(
      "input",
      "text",
      "ПІБ",
      "personality"
    );
    const Urgency = Elements.createFields(
      "select",
      "text",
      "терміновість",
      "urgency"
    );
    const urgencyOrdinary = Elements.createOptionFields("звичайна", false);
    const urgencyPriority = Elements.createOptionFields("пріоритетна", false);
    const urgencyUrgent = Elements.createOptionFields("невідкладна", false);

    Urgency[1].append(urgencyOrdinary, urgencyPriority, urgencyUrgent);
    return [...dateVisit, ...Purpose, ...Description, ...Personality, ...Urgency];
  }
}
export {ModalFormInput}