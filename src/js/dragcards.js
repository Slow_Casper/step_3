function dragCards(board) {
let draggedLi = null;

board.addEventListener('dragstart', function(event) {
  draggedLi = event.target.closest('li');
});

board.addEventListener('dragover', function(event) {
  event.preventDefault();
});

board.addEventListener('drop', function (event) {
    try {
        const TargetLi = event.target.closest('li')
        TargetLi.after(draggedLi)
    }
    catch { (error) => console.log(error, TargetLi) }
});

}
export {dragCards}