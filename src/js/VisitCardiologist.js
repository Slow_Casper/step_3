import { Visit } from "./Visit.js";
class VisitCardiologist extends Visit {
  constructor({
    modalbodyIndex,
    pressure,
    cardioDiseas,
    age,
    personality,
    doctor,
    purpose,
    description,
    urgency,
    id,
    visitData,
  }) {
    super({
      personality,
      doctor,
      purpose,
      description,
      urgency,
      id,
      visitData,
    });
    this.modalbodyIndex = modalbodyIndex;
    this.pressure = pressure;
    this.cardioDiseas = cardioDiseas;
    this.age = age;
  }

  render() {
    super.render();
    this.imgVisit.src =
      "https://res.cloudinary.com/djrrr9cpl/image/fetch/pg_1,e_outline:1,co_white/f_png,w_128,h_128/https%3A%2F%2Fportal-doctor.eleks.com%2Fapi%2Fgetfile%2Fdobrobutprodcms%2Fhome.jpg%3Fv%3Dt&quot";
    super.showMoreLess();

    this.infoVisit.innerHTML += `
    <p class="text__total">Індекс маси тіла: ${this.modalbodyIndex} .</p>
    <p class="text__total">Звичайний тиск: ${this.pressure} .</p>
    <p class="text__total">Перенесені захворювання CCC: ${this.cardioDiseas} .</p>
    <p class="text__total">Вік ${this.age} .</p>
    <p class="text__total">Дата відвідування: ${this.visitData} .</p>
    `;
    return this.li;
  }
}
export {VisitCardiologist}
