import { ModalFormInput } from "./ModalForm.js";
import { Elements } from "./elements.js";

class ModalFormInputCardiology extends ModalFormInput {
  constructor(form) {
    super();
    this.form = form;
  }

  createInputFieldCardiolog() {
    const Pressure = Elements.createFields(
      "input",
      "text",
      "звичайний тиск (мм.рт.ст)",
      "pressure"
    );
    const modalbodyIndex = Elements.createFields(
      "input",
      "number",
      "індекс маси тіла (кг/м2)",
      "modalbodyIndex"
    );
    const CardioDiseas = Elements.createFields(
      "input",
      "text",
      "перенесені захворювання серцево-судинної системи",
      "cardioDiseas"
    );
    const Age = Elements.createFields("input", "number", "вік", "age");
    this.form.append(
      ...this.createInputField(),
      ...Pressure,
      ...modalbodyIndex,
      ...CardioDiseas,
      ...Age
    );
  }
}
export {ModalFormInputCardiology}