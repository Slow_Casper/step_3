class ApiFetch {
  static async getToken(email, pass) {
    const response = await fetch(
      "https://ajax.test-danit.com/api/v2/cards/login",
      {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: email,
          password: pass,
        }),
      }
    );
    return response.text(); 
  }

  static async createPatient(token, jsonElements) {
    const response = await fetch("https://ajax.test-danit.com/api/v2/cards", {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify(jsonElements),
    });
    return response.json();
    }
    
    static async deleteCard(cardId){
      return await fetch(`https://ajax.test-danit.com/api/v2/cards/${cardId}`, {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${localStorage.getItem("tocken")}`,
        },
      }).then((response) => {
        if (!response.ok) {
          throw new Error("Bad response deleteCard from server");
        } else {
          return response;
        }
      });
  };

  static async changePatient(token, jsonElements, id) {
    const response = await fetch(
      `https://ajax.test-danit.com/api/v2/cards/${id}`,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(jsonElements),
      }
    );
    return response.json();
    }
    
  static async getCard(id, token) {
    const response = await fetch(
      `https://ajax.test-danit.com/api/v2/cards/${id}`,
      {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );
    return response.json();
  }



}
export {ApiFetch}