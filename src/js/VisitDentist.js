import { Visit } from "./Visit.js";
class VisitDentist extends Visit {
  constructor({
    last_visit,
    personality,
    doctor,
    purpose,
    description,
    urgency,
    id,
    visitData,
  }) {
    super({
      personality,
      doctor,
      purpose,
      description,
      urgency,
      id,
      visitData,
    });
    this.last_visit = last_visit;
    this.visitData = visitData;
  }

  render() {
    super.render();
    this.imgVisit.setAttribute(
      "src",
      "https://res.cloudinary.com/djrrr9cpl/image/fetch/pg_1,e_outline:1,co_white/f_png,w_128,h_128/https%3A%2F%2Fportal-doctor.eleks.com%2Fapi%2Fgetfile%2Fdobrobutprodcms%2Flandings%2F%D0%A1%D1%82%D0%BE%D0%BC%D0%B0%D1%82%D0%BE%D0%BB%D0%BE%D0%B3%D1%96%D1%8F%2FStomatologia.gif%3Fv%3D"
    );
    super.showMoreLess();

    this.infoVisit.innerHTML += `
    <p class="text__total">Дата останнього відвідування: ${this.last_visit} .</p>
    <p class="text__total">Дата відвідування: ${this.visitData} .</p>
    `;
    return this.li;
  }
}
export {VisitDentist}
