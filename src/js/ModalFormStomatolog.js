import { ModalFormInput } from "./ModalForm.js"
import { Elements } from "./elements.js"

class ModalFormInputStomatology extends ModalFormInput{
    constructor(form) {
        super()
        this.form = form
    }
    createInputFieldStomatolog() {
        const LastVisit = Elements.createFields('input', 'date','дата останнього відвідування', 'last_visit')
        this.form.append(...this.createInputField(), ...LastVisit)
    }
}
export {ModalFormInputStomatology}