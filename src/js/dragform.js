function dragAndDrop(card) {

    let currentCard = null;
    let xOffset = 0;
    let yOffset = 0;

    card.setAttribute('draggable', true)
    card.addEventListener('dragstart', dragStart);
    card.addEventListener('dragend', dragEnd);

    function dragStart(e) {
        currentCard = this;
        console.log(currentCard.offsetLeft, currentCard.offsetTop)
        console.log(e.clientX, e.clientY)
        xOffset = e.clientX - currentCard.offsetLeft;
        yOffset = e.clientY - currentCard.offsetTop;

    }

    function dragEnd(e) {
        currentCard = null;
    }

    document.addEventListener('dragover', dragOver);
    document.addEventListener('drop', dragDrop);

    function dragOver(e) {
        e.preventDefault();
    }

    function dragDrop(e) {
        if (currentCard) {
            currentCard.style.position = 'absolute';
            currentCard.style.zIndex = "5";
            currentCard.style.left = `${e.clientX - xOffset}px`;
            currentCard.style.top = `${e.clientY - yOffset}px`;
            console.log()
        }
    }
}
export {dragAndDrop}