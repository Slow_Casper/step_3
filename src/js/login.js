import {ApiFetch} from "./ajax.js"
import { getAllCards } from "./getCards.js";
import { loginBtn, closeBtn, loginForm , form} from "./elements.js";

function login(event) {
    event.preventDefault();
    const email = form.querySelector(".login__email").value;
    const password = form.querySelector(".login__password").value;

    ApiFetch.getToken(email, password)
        .then(tocken => {
            localStorage.setItem(`tocken`, `${tocken}`)
            getAllCards();
            console.log(tocken);
        })
    
    loginForm.classList.remove("login-active");
    loginBtn.style = "display: none";
    const createVisitBtn = document.querySelector(".main__buttons--visit");
    createVisitBtn.style = "display: block";
}


export {form, loginBtn, closeBtn, login, loginForm}
