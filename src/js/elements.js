class Elements{
    static createOptionFields(value, bool) {
        const option = document.createElement('option');
        option.textContent = value;
        option.disabled = bool;
        option.selected = bool;
        option.hidden = bool;
        return option
    }
    static  createButton(class_name, type, name) {
        const button = document.createElement('button');
        button.className = class_name;
        button.setAttribute('type', type);
        button.textContent = name;
        return button
    }
    static  createFields(field, type, value, name) {
        const label = document.createElement('label');
        label.className = 'modal-label';
        label.setAttribute('for', name);
        label.textContent = value;
        const input = document.createElement(field);
        input.className = 'modal-field';
        input.setAttribute('type', type);
        input.setAttribute('lang','uk-uk')
        input.name = name;
        if (type == 'number') {
            input.min = 0;
        }
        return [label, input]
    }
}

let allCardsArr = [];
const loginBtn = document.querySelector(".main__buttons--login");

const closeBtn = document.querySelector(".close");
const loginForm = document.querySelector(".login");

const buttonToOpen = document.querySelector('.main__buttons--visit');
const form = document.querySelector(".login__form");
const root = document.getElementById('root')
export {Elements, allCardsArr, buttonToOpen, loginBtn, closeBtn, loginForm, form, root}