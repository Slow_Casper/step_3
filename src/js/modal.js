import { ApiFetch } from "./ajax.js";
import { chooseRenderDoctor } from "./getCards.js";
import { Visit} from "./Visit.js";
import { dragAndDrop } from "./dragform.js";
import { Elements, buttonToOpen, root } from "./elements.js";
import { ModalFormInputCardiology } from "./ModalFormCardiology.js";
import { ModalFormInputStomatology } from "./ModalFormStomatolog.js";
import { ModalFormInputTerapevt } from "./ModalFormTerapevt.js";


class Modal {
    constructor(buttonModal, ...args) {
      this.buttonModal = buttonModal;
      this.args = args;
  }
 
  modalSubmit(button, form, modalbody) {
        const elementS = form.elements
        button.addEventListener('click', () => {
          if (!form.getAttribute('name')) {
              return;
          }
          else if (Array.from(elementS).some(el => el.value.trim() == '')) {
              Array.from(elementS).forEach(el => (el.value.trim() == '') ?
                  el.classList.add('error') :
                  el.classList.remove('error'));
          } else {
              const jsonElements = Array.from(elementS).reduce((acc, el) =>
                  Object.assign(acc, { [el.name]: el.value })
                  , { doctor: `${form.getAttribute('name')}`})
            
                  if (this.buttonModal===buttonToOpen) {
                    ApiFetch.createPatient(localStorage.getItem("tocken"), jsonElements)
                      .then(data => chooseRenderDoctor(data))
                      .catch(error => console.error(error))
                  } 
                  else {
                    const cardId = this.buttonModal.closest('.visit__menu').dataset.id
                    ApiFetch.changePatient(localStorage.getItem("tocken"), jsonElements, cardId)
                      .then(data => {
                        Visit.edit(data);
                    })
                      .catch(error => console.error(error));
                }
              modalbody.remove()
          }
        })
    }

  modalClose(button, modalbody) {
      window.addEventListener('click', (event) => {
        try {
          if (event.target.closest('div') !== modalbody &&
            event.target !== this.buttonModal &&
            event.target.closest('svg') != this.buttonModal) {
            modalbody.remove();
            }
        }
          catch { error => console.log(error) };
      })
      button.addEventListener('click', () => { modalbody.remove() });
  };
  
  getFieldInfo(form, data) {
          for (let key in form.elements) {
            if (data[key]) {
              form.elements[key].value = data[key];
            }
          }
        }
            
    
  getForm(form, doctorName) {
      form.innerHTML = '';
      if (doctorName == 'Кардіолог') {
        const card = new ModalFormInputCardiology(form);
        card.createInputFieldCardiolog();
      }
      else if (doctorName == 'Стоматолог') {
        const card = new ModalFormInputStomatology(form);
        card.createInputFieldStomatolog();
      }
      else if (doctorName == 'Терапевт') {
        const card = new ModalFormInputTerapevt(form);
        card.createInputFieldTerapevt();
      }
      form.name = doctorName;
      form.className = 'modal-form';
      };
    
  createModal(cardinfo) {
    const modalbody = document.createElement('div');
    modalbody.className = 'modal-body';
    const form = document.createElement('form');
    const doctorList = document.createElement('select');
    doctorList.className = 'modal-select';
    const labelDoctor = Elements.createOptionFields('Виберіть доктора', true);
    doctorList.append(labelDoctor);
    for (let doctor of this.args) {
      const doctorname = Elements.createOptionFields(doctor, false);
      doctorList.append(doctorname);
    }
    const buttonCreate = Elements.createButton('modal-create-button', 'submit', 'Cтворити');
    const buttonCansel = Elements.createButton('modal-cansel-button', 'button', 'Закрити');

    if (cardinfo) {
      const visitItem = this.buttonModal.closest('.visit__menu');
      const doctorName = visitItem.querySelector('.visit__text-doctor').textContent;
      doctorList.value = doctorName;
      this.getForm(form, doctorName);
      this.getFieldInfo(form, cardinfo);
      buttonCreate.textContent = 'Редагувати';
    } else {
      doctorList.addEventListener('change', (event) => {
        this.getForm(form, event.target.value);
      });  
    }
    this.modalSubmit(buttonCreate, form, modalbody);
    this.modalClose(buttonCansel, modalbody);
    modalbody.append(doctorList, form, buttonCansel, buttonCreate);
    root.append(modalbody);
    dragAndDrop(modalbody);
  }
}


export {Modal}