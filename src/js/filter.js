
import { getAOneCard} from "./getCards.js";
import { allCardsArr } from "./elements.js";

const search = document.querySelector(".filter__search");
const filterVisit = document.querySelector(".filter__visit");
const filterUrgency = document.querySelector(".filter__urgency");

function checkIsIncludes(text, query) {
    return text.toLowerCase().includes(query.toLowerCase())
}

function filterCards() {
    const li = document.querySelectorAll(".visit__menu");
    li.forEach((e) => {e.remove()})

    const filteredCards = allCardsArr.filter(el => {
        if (search.value && !checkIsIncludes(el.description, search.value) && !checkIsIncludes(el.purpose, search.value)) {
            return false
        }
        
        const isVisited = new Date(el.visitData).getTime() > Date.now();
        
        if (filterVisit.value !== 'open' && filterVisit.value !== 'all' && isVisited) {
            return false
        }

        if (filterVisit.value !== 'done' && filterVisit.value !== 'all' && !isVisited) {
            return false
        }

        if (filterUrgency.value !== 'all' && el.urgency !== filterUrgency.value) {
            return false
        }

        return true
    })

    filteredCards.forEach(el => getAOneCard(el.id))
}

export {search, filterVisit, filterUrgency, filterCards}