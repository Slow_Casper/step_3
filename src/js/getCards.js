import { ApiFetch } from "./ajax.js";
import { VisitCardiologist } from "./VisitCardiologist.js";
import { VisitDentist } from "./VisitDentist.js";
import { VisitTherapist } from "./VisitTherapist.js";
import { allCardsArr } from "./elements.js";

function getAllCards() {
  ApiFetch.getCard("", localStorage.getItem("tocken"))
    .then((data) => {
      data.forEach((data) => {
        if (
          data.doctor === "Кардіолог" ||
          data.doctor === "Стоматолог" ||
          data.doctor === "Терапевт"
        ) {
          const visitCard = chooseRenderDoctor(data);
          const listContainer = document.querySelector(".visit__list");
          listContainer.append(visitCard);
        }
      });
      allCardsArr.push(...data);
    });
}
function chooseRenderDoctor(data) {
  switch (data.doctor) {
    case "Кардіолог":
      const visitCardiologist = new VisitCardiologist(data).render();
      return visitCardiologist;
    case "Стоматолог":
      const visitDentist = new VisitDentist(data).render();
      return visitDentist;
    case "Терапевт":
      const visitTherapist = new VisitTherapist(data).render();
      return visitTherapist;
    default:
      console.log(data)
  }
  
}

function getAOneCard(id) {
  ApiFetch.getCard(id, localStorage.getItem("tocken"))
    .then((data) => {
      chooseRenderDoctor(data);
    });
}

export {getAOneCard, getAllCards, chooseRenderDoctor}