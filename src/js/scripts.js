import { Modal } from "./modal.js";
import {login} from "./login.js";
import {search, filterVisit, filterUrgency, filterCards } from "./filter.js";
import {buttonToOpen, loginBtn, closeBtn, form, loginForm,root } from "./elements.js";
import {getAllCards} from "./getCards.js"

loginBtn.addEventListener("click", () => {
    loginForm.classList.add("login-active");
})

closeBtn.addEventListener("click", () => {
    loginForm.classList.remove("login-active");
})


document.addEventListener("DOMContentLoaded", loadWin)

function loadWin() {
        
        if (localStorage.hasOwnProperty("tocken")) {
            loginBtn.style = "display: none";
            const createVisitBtn = document.querySelector(".main__buttons--visit");
            createVisitBtn.style = "display: block";
            getAllCards();
        }
}

form.addEventListener("submit", login)

search.addEventListener("change", () => { filterCards() });
filterVisit.addEventListener("change", filterCards);
filterUrgency.addEventListener("change", filterCards);

buttonToOpen.addEventListener('click', () => {
    if (!root.hasChildNodes()) {
        const modalinstance = new Modal(buttonToOpen, 'Кардіолог', 'Стоматолог', 'Терапевт')
        modalinstance.createModal(null);
    }
})
